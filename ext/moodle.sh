#!/bin/bash

logger "Mounting share"
if [ ! -e $MOO_MOUNT/mount.info ]; then
    mount -t cifs -o username=$MOO_USER,domain=$MOO_DOMAIN,password=$MOO_PASS $MOO_SHARE $MOO_MOUNT > /dev/null 2> /dev/null 
    if [ $? -gt 0 ]; then
        logger "Nepodařilo se připojit share $MOO_SHARE"
        sudden_exit
    fi
else
    logger "Už je připojeno."
fi

if [ ! -e $MOO_MOUNT/mount.info ]; then
    logger "Nemohu najít soubor mount.info, zalohu nelze dokončit."
    sudden_exit
fi

BUROOT=$MOO_MOUNT

logger "Mazání starých záloh"
OLD=$[`date +%s`-$LASTS]
if [ $LASTS -gt 0 ]; then
    for t in $BUROOT/*; do
        if [ -d "$t" ]; then
            TIMESTAMP=`echo $t | perl -ne 'print $1 if /.*([0-9]{10}).*/'`
            if [ $TIMESTAMP ]; then
                if [ $TIMESTAMP -lt $OLD ]; then
                    if [ `echo $t | grep "$BUROOT"` ]; then
                        logger "-- $t"
                        rm -rf $t
                    fi
                fi
            fi
        fi
    done
fi

BUDIR=$BUROOT/`date +%s`
mkdir "$BUDIR"

if [ ! -e "$BUDIR" ]; then
    logger "Chyba! Neexistuje $BUIDR"
    sudden_exit
fi

logger "Kopirovani"
for srv in ${SERVERS[*]}; do
    if [ -e $SERVDIR/$srv ]; then
        logger "-- $srv"
        mv $SERVDIR/$srv $BUDIR
    fi
done

logger "Umounting share"
umount $MOO_MOUNT

