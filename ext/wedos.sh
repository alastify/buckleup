#!/bin/bash

WOUT=0

if [ ! -e "$BUROOT" ]; then
    mount /mnt/wedos
    mntest=`mount | grep s2558`

    if [ -z "$mntest" ];  then
        logger "Nepodarilo se pripojit zalozni disk"
        sudden_exit
    else
        logger "Připojeno vzdálené úložiště"
    fi
    
    /sbin/losetup /dev/loop0 /mnt/wedos/data
    if [ $? -ne 0 ]; then
        logger "Chyba losetup"
        sudden_exit
    fi
    
    /sbin/cryptsetup --key-file $WEDOSKEY luksOpen /dev/loop0 mydata
    if [ $? -ne 0 ]; then
        logger "Chyba cryptsetup"
        sudden_exit
    fi
    
    mount /dev/mapper/mydata /mnt/backup/
    if [ $? -eq 0 ]; then
        logger "Odemčeno a mountováno"
        WOUT=1
    else
        logger "Chyba! Nepodařilo se odemknout a připojit."
        sudden_exit
    fi
fi

if [ ! -e "$BUROOT" ]; then
    logger "$BUROOT neexistuje"
    sudden_exit
fi

## cleanup

logger "Mazání starých záloh"
OLD=$[`date +%s`-$LASTS]
if [ $LASTS -gt 0 ]; then
    for t in $BUROOT/*; do
        if [ -d "$t" ]; then
            TIMESTAMP=`echo $t | perl -ne 'print $1 if /.*([0-9]{10}).*/'`
            if [ $TIMESTAMP ]; then
                if [ $TIMESTAMP -lt $OLD ]; then
                    if [ `echo $t | grep "$BUROOT"` ]; then
                        logger "-- $t"
                        rm -rf $t
                    fi
                fi
            fi
        fi
    done
fi

## volné místo
logger "Stav uloziste"
logger "  `df -h | head -1`"
logger "  `df -h | grep /mnt/backup`"

## copy

BUDIR=$BUROOT/`date +%s`
mkdir "$BUDIR"

if [ ! -e "$BUDIR" ]; then
    logger "Chyba! Neexistuje $BUIDR"
fi

logger "Kopirovani"
for srv in ${SERVERS[*]}; do
    if [ -e $SERVDIR/$srv ]; then
        logger "-- $srv"
        mv $SERVDIR/$srv $BUDIR
    fi
done

## volné místo
logger "Stav uloziste"
logger "  `df -h | head -1`"
logger "  `df -h | grep /mnt/backup`"

if [ $WOUT -eq 1 ]; then
    umount /mnt/backup
    /sbin/cryptsetup luksClose mydata
    logger "Zamčeno"
    /sbin/losetup -d /dev/loop0
    umount /mnt/wedos
    logger "Odpojeno"
fi



#pocet=`ls -1 $BACKUP_ROOT | wc -l`
#if [ $pocet -gt $MAXBU ]; then
#  echo "POZOR! Zalohy nejsou inkrementalni."
#  echo "Na zaloznim serveru je uz $pocet zaloh, bude potreba nektere rucne odmazat."
#fi
