#!/bin/bash

# Hlavni konfiguracni soubor "config" se hleda v domovskem adresari $CFGDIR
# a nebo v adresari, kde je skript backup.sh
#
# Každé rozšíření může mít svůj konfigurační soubor s názvem
# "config-rozsireni"
#

# adresar s konfiguraci
CFGDIR=~/.buckleup
CFGFILE=$CFGDIR/config
ZSCRIPT=$( cd "$( dirname "$0" )" && pwd )
cd $ZSCRIPT

. common

for hook in `ls -1 pre-hooks`; do
    if [ -x "pre-hooks/$hook" ]; then
        #logger "Hook: $hook"
        . pre-hooks/$hook
    fi
done

# hlavicka zalohy
logger "`hostname` (`date +%F`)"
logger "config: $CFGFILE"

# poradi v jakem se nactou nejprve pluginy a pak rozsireni
tasks=( 'plugins' 'ext' )

for task in ${tasks[*]}; do
    # nacteni skiptu v dane uloze
    for t in "$ZSCRIPT"/$task-enabled/*.sh; do
        # kazdy skript muze mit svuj konfiguracni soubor
        cfg="$CFGDIR/config-`basename "$t" .sh`"
        if [ -f "$t" ]; then
            logger "# `basename $t .sh`"
            if [ -f "$cfg" ]; then
                . "$cfg"
            else
                cfg="$ZSCRIPT"/$task/config-`basename "$t" .sh`
                if [ -f $cfg ]; then
                    . $cfg 2> /tmp/bu-$PID-errors.log
                    errors=`cat /tmp/bu-$PID-errors.log`
                    if [ "$errors" ]; then
                        logger $errors
                    fi
                else
                    cfg=""
                fi
            fi

            if [ $cfg ]; then
                logger "config: $cfg"
            fi

            # spusteni ulohy
            . "$t"
        fi
    done
done

cd $ZSCRIPT
for hook in `ls -1 post-hooks`; do
    if [ -x "post-hooks/$hook" ]; then
        #logger "Hook: $hook"
        . post-hooks/$hook
    fi
done


exit 0
