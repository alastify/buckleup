#!/bin/bash

cd /
bfile=$BACKUP_PATH/srv.tar.gz
tar -zcf $bfile srv/
if [ $? -gt 0 ]; then
    logger "Error! Při kompresi $bfile"
fi
