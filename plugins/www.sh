#!/bin/bash

service apache2 stop > /dev/null 2> /dev/null

for v in "${VHOSTS[@]}"; do
    bfile="$BACKUP_PATH/vhost-`echo $v | sed -e 's/\//./g'`.tar.gz"
    logger " -- vhost: $v"
    cd $WWWDIR
    tar -zcf $bfile $v
    if [ $? -gt 0 ]; then
        logger "Error! Při kompresi $bfile"
    fi
done

service apache2 start > /dev/null 2> /dev/null
