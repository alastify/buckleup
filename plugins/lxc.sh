
#!/bin/bash

mkdir $BACKUP_PATH/lxc/

for l in "${LXCS[@]}"; do
  logger " -- lxc: $l"
  bfile=$BACKUP_PATH/lxc/$l-config
  cp $LXCS_DIR/$l/config $bfile
  if [ $? -gt 0 ]; then
      logger "Error! Copy $bfile."
  fi
done
