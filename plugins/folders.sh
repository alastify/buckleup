#!/bin/bash

for v in "${FOLDERS_PATHS[@]}"; do
    if [ -e $v ]; then
        bfile="$BACKUP_PATH/folder`echo $v | sed -e 's/\//./g'`.tar.gz"
        logger " -- part: $v"
        tar $TAR_PARAMS -zcf $bfile $v > /dev/null 2> /dev/null
        if [ $? -gt 0 ]; then
            logger "Error! Při kompresi $bfile"
        fi
    else
        logger " -- part: $v does not exists, skipping"
    fi
done
