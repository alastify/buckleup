#!/bin/bash

cd /
bfile=$BACKUP_PATH/home.tar.gz
tar -zcf $bfile home/
if [ $? -gt 0 ]; then
    logger "Error! Při kompresi $bfile"
fi
