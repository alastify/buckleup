#!/bin/bash

service apache2 stop > /dev/null 2> /dev/null

cd /srv
bfile="$BACKUP_PATH/owncloud.tar.gz"
tar -zcf $bfile owncloud
if [ $? -gt 0 ]; then
    logger "Error! Při kompresi $bfile"
fi

service apache2 start > /dev/null 2> /dev/null
