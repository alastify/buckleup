#!/bin/bash

cd /
bfile=$BACKUP_PATH/etc.tar.gz
tar -zcf $bfile etc/
if [ $? -gt 0 ]; then
    logger "Error! Při kompresi $bfile"
fi
