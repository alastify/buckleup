#!/bin/bash

PLONE_CWD=`pwd`

#
sudo -u $PLONE_USER $PLONE_PATH/$PLONE_DIRNAME/zeocluster/bin/plonectl stop > /dev/null 2> /dev/null

cd $PLONE_PATH;
bfile="$BACKUP_PATH/plone.tar.gz"
tar -zcf $bfile $PLONE_DIRNAME/
if [ $? -gt 0 ]; then
    logger "Error! Při kompresi $bfile"
fi

cd $PLONE_CWD;

sudo -u $PLONE_USER $PLONE_PATH/$PLONE_DIRNAME/zeocluster/bin/plonectl start > /dev/null 2> /dev/null
