#!/bin/bash

if [ -e $GITDIR ]; then
    cd $GITDIR
    tar -zcf $BACKUP_PATH/git.tar.gz git/
    if [ $? -gt 0 ]; then
        logger "Error! Při kompresi $bfile"
    fi
else
    logger "Error! $GITDIR neexistuje."
fi
