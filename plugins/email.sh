#!/bin/bash

MA_BACKUP=$BACKUP_PATH/email

mkdir $MA_BACKUP

if [ -e $MAILDIR ]; then
    cd $MAILDIR

    for m in "${MAILBOXES[@]}"; do
        if [ -e $m ]; then
            logger " -- mail: $m"
            bfile="$MA_BACKUP/`echo -n $m | sed 's/@/./g'`.tar.gz"
            tar -zcf $bfile "$m/"
            if [ $? -gt 0 ]; then
                logger "Error! Při kompresi $bfile"
            fi
        else
            logger "Error! Adresář $m neexistuje, vynechávám"
        fi
    done
else
    logger "Error! $MAILDIR neexistuje."
fi
