#!/bin/bash

service lighttpd stop > /dev/null 2> /dev/null
service vsftpd stop > /dev/null 2> /dev/null

for v in "${WH_PARTS[@]}"; do
    bfile="$BACKUP_PATH/webhosting-`echo $v | sed -e 's/\//./g'`.tar.gz"
    if [ -e WH_DIR ]; then
        cd $WH_DIR
        if [ -e $WH_DIR/$v ]; then
            logger " -- part: $v"
            tar -zcf $bfile $v > /dev/null 2> /dev/null
            if [ $? -gt 0 ]; then
                logger "ERROR při kompresi $bfile"
            fi
        else
            logger " -- part: $v does not exists, skipping"
        fi
    else
        logger "Not found: $WH_DIR"
    fi
done

service lighttpd start > /dev/null 2> /dev/null
service vsftpd start > /dev/null 2> /dev/null
