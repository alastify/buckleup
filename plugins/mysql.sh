#!/bin/bash

MY_BACKUP=$BACKUP_PATH/mysql

mkdir $MY_BACKUP

for d in "${DATABASES[@]}"; do
    logger " -- mysql: $d"
    bfile=$MY_BACKUP/$d.dump.sql.gz
    mysqldump -u root -C -Q -e --events --create-options $d | gzip -c > $bfile
    if [ $? -gt 0 ]; then
        logger "Error! Při mysqldump."
    fi
done
