#!/bin/bash

PG_BACKUP=$BACKUP_PATH/pgsql

mkdir $PG_BACKUP
chown $DBUSER $PG_BACKUP

for d in "${DATABASES[@]}"; do
    logger " -- postgres: $d"
    bfile=$PG_BACKUP/$d.dump
    # pg_dump -U $DBUSER -F c -b -f $bfile $d
    su $DBUSER -c "cd / && pg_dump -F c -b -f $bfile $d"
    if [ $? -gt 0 ]; then
        logger "Error! Při pg_dump."
    fi
done
