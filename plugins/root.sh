#!/bin/bash

cd /
bfile=$BACKUP_PATH/root.tar.gz
tar -zcf $bfile root/
if [ $? -gt 0 ]; then
    logger "Error! Při kompresi $bfile"
fi
