#!/bin/bash

if [ -e $GITHOME ]; then
    cd $GITHOME
    GBLOG=$BACKUP_PATH/gitlab-backup.log
    gitlab-rake gitlab:backup:create > $GBLOG 2> $GBLOG

    GBF=`ls -1 | sort | tail -1`

    LASTB=$GITHOME/$GBF
    if [ -e $LASTB ]; then
        bfile=$BACKUP_PATH/$GBF.gz
        gzip -c "$LASTB" > $bfile
        if [ $? -gt 0 ]; then
            logger "Error! Při kompresi $bfile"
        fi
    fi
else
    logger "Error! $GITDIR neexistuje."
fi
