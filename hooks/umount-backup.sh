#!/bin/bash

STORAGECFGFILE=$CFGDIR/config-mount

if [ -e "$STORAGECFGFILE" ]; then
    . "$STORAGECFGFILE"
else
    STORAGECFGFILE="config-mount"
    if [ -e $STORAGECFGFILE ]; then
        . $STORAGECFGFILE
    else
        logger "Error! $STORAGECFGFILE not found!"
        sudden_exit
    fi
fi

umount $TARGET
if [ $? -gt 0 ]; then
    logger "Nepodařilo se odpojit $TARGET"
    sudden_exit
fi
