#!/bin/bash

# test zámku
LOCKFILE=$BACKUP_PATH/.buckle-lock
if [ -e "$LOCKFILE" ]; then
    logger "WTF?!? There is an active lock-file!!!"
    sudden_exit
else
    touch $LOCKFILE
fi

if [ ! -e "$LOCKFILE" ]; then
    logger "Could not create lock-file: $LOCKFILE"
    sudden_exit
fi
