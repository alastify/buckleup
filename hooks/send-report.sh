#!/bin/bash

if [ -e "$LOGFILE" ]; then
    /usr/bin/mail -s "Backup complete - `hostname`" $BEMAIL < $LOGFILE

    if [ "$BEMAIL_CC" ]; then
        /usr/bin/mail -s "Backup complete - `hostname`" $BEMAIL_CC < $LOGFILE
    fi
fi
