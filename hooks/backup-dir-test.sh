#!/bin/bash

# test adresare se zalohou
if [ ! -e "$BACKUP_PATH" ]; then
    mkdir "$BACKUP_PATH" 2> /dev/null
    if [ $? -ne 0 ]; then
        logger "Error! Creating: $BACKUP_PATH"
        sudden_exit
    fi
fi
