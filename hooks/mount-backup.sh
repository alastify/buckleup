#!/bin/bash

STORAGECFGFILE=$CFGDIR/config-mount

if [ -e "$STORAGECFGFILE" ]; then
    . "$STORAGECFGFILE"
else
    STORAGECFGFILE="config-mount"
    if [ -e $STORAGECFGFILE ]; then
        . $STORAGECFGFILE
    else
        logger "Error! $STORAGECFGFILE not found!"
        sudden_exit
    fi
fi

#@override
BACKUP_ROOT=$TARGET

if [ ! -e "$BACKUP_ROOT/mount.info" ]; then
	mount -t $FSTYPE -o $OPTIONS $DEVICE $TARGET > /dev/null 2> /dev/null
    if [ $? -gt 0 ]; then
        logger "Nepodařilo se připojit $DEVICE"
        sudden_exit
    fi
fi

ABSPATH=$TARGET

touch $ABSPATH/mount.info
if [ $? -gt 0 ]; then
    logger "Nepodařilo se vytvořit $ABSPATH/mount.info"
    umount $TARGET
    sudden_exit
fi

if [ "$SUBDIR" ]; then
	ABSPATH=$TARGET/$SUBDIR
fi

if [ $USE_DATESTAMP -eq 1 ]; then
	ABSPATH="$ABSPATH/`date +"%Y-%m-%d"`"
fi

if [ $USE_DATESTAMP -eq 1 ]; then
	if [ ! -e $ABSPATH ]; then
		mkdir $ABSPATH
		if [ $? -gt 0 ]; then
	        logger "Nepodařilo se vytvořit $ABSPATH"
	        umount $TARGET
	        sudden_exit
	    fi
    fi
fi

#@override
BACKUP_PATH=$ABSPATH/$BACKUP_DIR
