#!/bin/bash

# test připojeného úložiště
if [ ! -e "$BACKUP_ROOT/mount.info" ]; then
    logger "Error! mount.info not found in : $BACKUP_ROOT"
    sudden_exit
fi
