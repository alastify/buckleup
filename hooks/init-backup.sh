#!/bin/bash

STORAGECFGFILE=$CFGDIR/config-init

if [ -e "$STORAGECFGFILE" ]; then
    . "$STORAGECFGFILE"
else
    STORAGECFGFILE="config-init"
    if [ -e $STORAGECFGFILE ]; then
        . $STORAGECFGFILE
    else
        logger "Error! $STORAGECFGFILE not found!"
        sudden_exit
    fi
fi


ABSPATH=$BACKUP_ROOT

if [ $USE_DATESTAMP -eq 1 ]; then
        ABSPATH="$ABSPATH/`date +"%Y-%m-%d"`"
fi

if [ $USE_DATESTAMP -eq 1 ]; then
        if [ ! -e $ABSPATH ]; then
                mkdir $ABSPATH
                if [ $? -gt 0 ]; then
                        logger "Nepodařilo se vytvořit $ABSPATH"
                        sudden_exit
                fi
        fi
fi

#@override
BACKUP_PATH=$ABSPATH/$BACKUP_DIR
