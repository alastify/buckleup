#!/bin/bash

STORAGECFGFILE=$CFGDIR/config-storage

if [ -e "$STORAGECFGFILE" ]; then
    . "$STORAGECFGFILE"
else
    STORAGECFGFILE="config-storage"
    if [ -e $STORAGECFGFILE ]; then
        . $STORAGECFGFILE
    else
        logger "Error! config-storage not found!"
        sudden_exit
    fi
fi

if [ "$LINK" ]; then
	rm -f $LINK
fi

if [ "$NFS_RESET" ]; then
	exportfs -r
fi

umount $TARGET
if [ $? -gt 0 ]; then
    logger "Nepodařilo se odpojit $TARGET"
    sudden_exit
fi
