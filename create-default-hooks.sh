#!/bin/bash

CFGDIR=~/.buckleup
CFGFILE=$CFGDIR/config
ZSCRIPT=$( cd "$( dirname "$0" )" && pwd )
cd $ZSCRIPT

. common

# PRE-HOOKS
HOOK_LIST=(
	"mount-backup.sh" \
	"mount-info.sh" \
	"backup-dir-test.sh" \
	"lock-create.sh" \
)

cd $ZSCRIPT/pre-hooks
OFFSET=10
for hook in "${HOOK_LIST[@]}"; do
	ln -s ../hooks/$hook "$OFFSET-$hook"
	OFFSET=$((OFFSET + 10))
done

# POST-HOOKS
HOOK_LIST=(
	"lock-destroy.sh" \
	"done.sh" \
	"send-report.sh" \
	"umount-backup.sh" \
)

cd $ZSCRIPT/post-hooks
OFFSET=10
for hook in "${HOOK_LIST[@]}"; do
	ln -s ../hooks/$hook "$OFFSET-$hook"
	OFFSET=$((OFFSET + 10))
done

cd $ZSCRIPT
